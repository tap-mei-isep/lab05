package vending

import DomainError.*
import scala.annotation.targetName

// TODO: Create the simple types
//       On name collision (two opaque types in the same file are based on the same type and have equal extension methods),
//       use the  @targetName annotation.
object SimpleTypes:
  
  type Result[A] = Either[DomainError,A]

  // TODO: Create the opaque type Money, which is an Integer and can never be negative
  // TODO: Extension method to, which returns the corresponding Int
  // TODO: Extension methods for comparison ( <, > ) between Money
  // TODO: Extension methods for math ( +, -, * ) between Money
  // TODO: Extension methods for math ( / ), division by Denomination, returning a Quantity
  // TODO: Extension method isZero, returning a Boolean 
  opaque type Money = Int
  
  // TODO: Create the opaque type Quantity,  which is an Integer and can never be negative
  // TODO: Extension method to, which returns the corresponding Int
  // TODO: Extension method isZero should also be created for this type
  // TODO: infix Extension method min, which returns the min Quantity
  opaque type Quantity = Int

  
  // TODO: Create an enum type for Denomination in which each case has a value in cents
  // TODO: Cases must be created for 1,2,5,10,20,50,100,200 (in cents)
  enum Denomination(val value: Money):
    case oneCent extends Denomination(1)
